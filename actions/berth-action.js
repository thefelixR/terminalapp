import {
  BERTH_ALL_LOCATIONS,
  BERTH_ADD_FAVORITE,
  BERTH_REMOVE_FAVORITE,
  BERTH_SEARCH,
  BERTH_SELECT,
} from './types'
import { portCDMbaseURL, portCDMheaders } from '../config/portcdmAPI'

export const berth_get_all_locations = _ => dispatch => {

  let allBerths;

  fetch(portCDMbaseURL + '/location-registry/locations', {
    method: 'GET',
    headers: portCDMheaders
  })
    .then(res =>
      res.json()
    )
    .then(jsonArray => {  // all berth locations wrapped in an array
      allBerths = jsonArray.map(berth => {
        return {...berth, isFavorite: false}
      });

      dispatch({
        type: BERTH_ALL_LOCATIONS,
        payload: allBerths
      });

    })
    .catch(err => {
      console.log("All berths error: " + err);
    })
}

export const berth_add_favorite = index => dispatch => {
  dispatch({
    type: BERTH_ADD_FAVORITE,
    payload: index
  })
}

export const berth_remove_favorite = index => dispatch => {
  dispatch({
    type: BERTH_REMOVE_FAVORITE,
    payload: index
  })
}

export const berth_search = query => dispatch => {
  dispatch({
    type: BERTH_SEARCH,
    payload: berth_index
  });
}

export const berth_select = berth => dispatch => {
  dispatch({
    type: BERTH_SELECT,
    payload: berth
  });
}