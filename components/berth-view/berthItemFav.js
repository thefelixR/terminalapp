import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import { berth_add_favorite, berth_remove_favorite } from '../../actions'
import { connect } from 'react-redux';

class BerthItemGeneral extends Component {

  constructor(props) {
    super(props)
  }

  toggle_favorite_berth(index) {
    this.props.allBerths[index].isFavorite ?
      this.props.berth_remove_favorite(index) :
      this.props.berth_add_favorite(index)
  }

  render() {
    const { name, index, allBerths } = this.props

    return (
      <TouchableOpacity style={s.container}
        onPress={() => {
          this.toggle_favorite_berth(index);
        }}
      >
        <Text style={s.txt}>{name}</Text>
        <Icon
          name='heart'
          type='font-awesome'
          color={allBerths[index].isFavorite ? 'red' : 'grey'}
          size={20}
        />
      </TouchableOpacity>
    )
  }
}

  const s = StyleSheet.create({
    container: {
      height: 50,
      borderColor: 'black',
      borderBottomWidth: 1,
      justifyContent: 'space-between',
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 10,
      marginLeft: 10
    },
    txt: {
      fontSize: 20,
    }

  });


  const mapStateToProps = (state) => {
    return {
      allBerths: state.berth_reducer.allBerths
    }
  }

  export default connect(mapStateToProps, {
    berth_add_favorite,
    berth_remove_favorite
  })(BerthItemGeneral);