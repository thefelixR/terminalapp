import React, { Component } from 'react';
import { View, StyleSheet, FlatList, Text, Button } from 'react-native';
import Header from '../header-view';
import { connect } from 'react-redux';
import { berth_get_all_locations } from '../../actions';
import Swiper from 'react-native-swiper';
import BerthItem from './berthItem';
import BerthItemFav from './berthItemFav';

class BerthView extends Component {

  constructor(props) {
    super(props)
    this.state = {
      query: '',
      result: ''
    }
  }

  // Get all locations when u start the app
  // TODO move this to where app is initilized
  loadBerths() {
    this.props.berth_get_all_locations();
  }

  render() {

    const { allBerths } = this.props;
    return (
      <View style={s.container}>
        <View style={s.container}>
          <Header viewName={'All berths'} />
          <Button title="get berths" onPress={() => this.loadBerths()} />
          {
            allBerths.length > 0 &&
            <Swiper style={s.swiperContainer}>
              <FlatList
                data={allBerths}
                renderItem={({ item, index }) =>
                  <BerthItem
                    name={item.name}
                    index={index}
                  />
                }
                keyExtractor={(item, index) => index.toString()}
              />
              <FlatList
                data={allBerths}
                renderItem={({ item, index }) =>
                  <BerthItemFav
                    name={item.name}
                    index={index}
                  />
                }
                keyExtractor={(item, index) => index.toString()}
              />
            </Swiper>
          }
        </View >
      </View >
    )
  }
}

const s = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const mapStateToProps = (state) => {
  return {
    allBerths: state.berth_reducer.allBerths,
  }
}

export default connect(mapStateToProps, {
  berth_get_all_locations
})(BerthView);