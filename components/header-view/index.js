import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import colorScheme from '../../config/colors';


class Header extends Component {


  render() {

    const { viewName } = this.props;

    return (
      <View style={s.container}>
        <Text style={s.txt}>{viewName}</Text>
      </View>
    )
  }
}

const s = StyleSheet.create({
  container: {
    width: '100%',
    paddingTop: 20,
    height: 70,
    backgroundColor: colorScheme.primaryColor,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txt: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  }

});

export default Header;