import React, { Component } from 'react'
import { Provider } from 'react-redux'
import configureStore from './store'
import BerthSearch from './components/berth-view'

const store = configureStore();

class TerminalApp extends Component {
  render() {
    return (
      <Provider store={store}>
        <BerthSearch />
      </Provider>
    );
  }
} 

export default TerminalApp;