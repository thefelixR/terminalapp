import { combineReducers } from 'redux';
import berth_reducer from './berth-reducers';

const rootReducer = combineReducers({
  berth_reducer
})

export default rootReducer