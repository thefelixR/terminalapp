import {
  BERTH_ALL_LOCATIONS,
  BERTH_ADD_FAVORITE,
  BERTH_REMOVE_FAVORITE,
  BERTH_SELECT,
  BERTH_SEARCH,
} from '../actions/types'

const initalState = {
  selectedBerth: null,
  allBerths: [
    // {
    //   "URN": "urn:mrn:stm:location:SEGOT:BERTH:rya562",
    //   "aliases": [
    //     "562",
    //     "Rya 562",
    //     "Kaj 562"
    //   ],
    //   "area": {
    //     "coordinates": []
    //   },
    //   "isFavorite": false,
    //   "locationType": "BERTH",
    //   "name": "Rya Harbour 562",
    //   "position": {
    //     "latitude": "0.0",
    //     "longitude": "0.0"
    //   },
    //   "shortName": "56§2"
    // }    
  ],
}

const berth_reducer = (state = initalState, action) => {
  switch (action.type) {

    case BERTH_ALL_LOCATIONS:
      return {
        ...state,
        allBerths: action.payload
      }

    case BERTH_ADD_FAVORITE: {
      let index = action.payload;
      return {
        ...state,
        allBerths: [...state.allBerths,
          state.allBerths[index].isFavorite = true
        ]
      }
    }

    case BERTH_REMOVE_FAVORITE: {
      let index = action.payload;
      return {
        ...state,
        allBerths: [...state.allBerths,
          state.allBerths[index].isFavorite = false
        ]
      }
    }

    case BERTH_SEARCH:
      return state

    case BERTH_SELECT:
      return {
        ...state,
        selectedBerth: action.payload
      }

    default:
      return state
  }
}

export default berth_reducer;